var Velocity = function() {
    this.data = {
        width: 720,
        height: 480
    };
    
    VelocityShell.log("Hi!");
    VelocityShell.setColor(255, 255, 255);
	VelocityShell.fillRect(0, 0, this.data.width, this.data.height);
    VelocityShell.setColor(200, 200, 200);
	VelocityShell.fillRect(0, 0, this.data.width, 40);
	VelocityShell.repaint();
};

Velocity.prototype.renderHud = function() {
    VelocityShell.setColor(200, 200, 200);
	VelocityShell.fillRect(0, 0, this.data.width, 50);
};

Velocity.prototype.reset = function() {
    VelocityShell.setColor(255, 255, 255);
	VelocityShell.fillRect(0, 0, this.data.width, this.data.height);
};

var v = new Velocity();
