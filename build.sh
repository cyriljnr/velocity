#CMD: build 'core' 'shell' 'version'
if [ $# -lt 3 ]
then
    echo "~~~~~~ MISSING PARAMETERS ~~~~~"
    echo "Please provide the core, shell, and version to export as."
    echo "example: 'sh build.sh js java-pc 1_0_0'"
    exit
fi

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "|    Velocity Assembler v1    |"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "Using the '$1' core."
echo "Using the '$2' shell."
echo "Exporting as version '$3'."
mkdir ./build/
mkdir ./build/$3/
echo "Constructing Java shell..."
cd ./shell/$2/
mvn clean dependency:copy-dependencies package
mv ./target/velocity-0.0.1-SNAPSHOT.jar ../Builds/java-pc/shell.jar
cd ../..
cp ./shell/Builds/java-pc/shell.jar ./build/$3/
echo "Java shell constructed."

if [ $1 = "js" ]
then
    echo "Constructing js core..."
    cp core/$1/* ./build/$3/
    echo "Js core constructed."
    
fi

echo "Build constructed sucessfully!"
echo "Run the 'shell.jar' script in './build/$3/' to run Velocity"