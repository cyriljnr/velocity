package info.gamefruit.velocity;

import java.awt.Color;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import com.eclipsesource.v8.JavaCallback;
import com.eclipsesource.v8.JavaVoidCallback;
import com.eclipsesource.v8.V8;
import com.eclipsesource.v8.V8Array;
import com.eclipsesource.v8.V8Object;

import info.gamefruit.velocity.elements.Window;

public class Core {
	public static V8 runtime;
	public static void main(String args[]) {
		BufferedReader br = null;
		String data = "";
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader("velocity.js"));
			while ((sCurrentLine = br.readLine()) != null) {
				data += sCurrentLine;
			}
		} catch (IOException e) {
			e.printStackTrace();
			data = "1+1"; // don't provide null! Avoid the 'undefined' error
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		runtime = V8.createV8Runtime();
		final Window win = new Window(720, 480);
		final V8Object renderer = new V8Object(runtime);
		JavaVoidCallback veloShellLog = new JavaVoidCallback() {
			public void invoke(final V8Object receiver, final V8Array args) {
				System.out.println(args.get(0));
				args.release();
				receiver.release();
			}
		};
		
		JavaVoidCallback veloShellFillRect = new JavaVoidCallback() {
			public void invoke(final V8Object receiver, final V8Array args) {
				if(args.length() > 3) {
					win.pane.g.fillRect(args.getInteger(0), args.getInteger(1), args.getInteger(2), args.getInteger(3));
				}
				
				args.release();
				receiver.release();
			}
		};
		
		JavaVoidCallback veloShellDrawRect = new JavaVoidCallback() {
			public void invoke(final V8Object receiver, final V8Array args) {
				if(args.length() > 3) {
					win.pane.g.drawRect(args.getInteger(0), args.getInteger(1), args.getInteger(2), args.getInteger(3));
				}
				
				args.release();
				receiver.release();
			}
		};
		
		JavaVoidCallback veloShellSetColor = new JavaVoidCallback() {
			public void invoke(final V8Object receiver, final V8Array args) {
				if(args.length() == 3) {
					win.pane.g.setColor(new Color(args.getInteger(0), args.getInteger(1), args.getInteger(2)));
				} else if(args.length() == 4) {
					win.pane.g.setColor(new Color(args.getInteger(0), args.getInteger(1), args.getInteger(2), args.getInteger(3)));
				}
				
				args.release();
				receiver.release();
			}
		};
		
		JavaVoidCallback veloShellRepaint = new JavaVoidCallback() {
			public void invoke(final V8Object receiver, final V8Array args) {
				win.pane.repaint();
				args.release();
				receiver.release();
			}
		};
		
		JavaVoidCallback veloShellDrawString = new JavaVoidCallback() {
			public void invoke(final V8Object receiver, final V8Array args) {
				if(args.length() > 2) {
					win.pane.g.drawString(args.getString(0), args.getInteger(1), args.getInteger(2));
				}
				
				args.release();
				receiver.release();
			}
		};
		
		JavaVoidCallback veloShellSetFont = new JavaVoidCallback() {
			public void invoke(final V8Object receiver, final V8Array args) {
				if(args.length() > 0) {
					win.pane.g.setFont(new Font(args.getString(0), args.getInteger(1), Font.PLAIN));
				}
				
				args.release();
				receiver.release();
			}
		};

		JavaCallback veloShellGetData = new JavaCallback() {
			public Object invoke(final V8Object receiver, final V8Array args) {
				final V8Object resp = new V8Object(runtime);
				resp.add("width", win.pane.getWidth());
				resp.add("height", win.pane.getHeight());
				args.release();
				receiver.release();
				return new V8Object(runtime);
			}
		};

		renderer.registerJavaMethod(veloShellFillRect, "fillRect");
		renderer.registerJavaMethod(veloShellDrawRect, "drawRect");
		renderer.registerJavaMethod(veloShellDrawString, "drawString");
		renderer.registerJavaMethod(veloShellSetColor, "setColor");
		renderer.registerJavaMethod(veloShellSetFont, "setFont");
		renderer.registerJavaMethod(veloShellRepaint, "repaint");
		renderer.registerJavaMethod(veloShellGetData, "getData");
		renderer.registerJavaMethod(veloShellLog, "log");
		runtime.add("VelocityShell", renderer);
		renderer.release();
		runtime.executeScript(data);
		runtime.release(true);
	}
}
