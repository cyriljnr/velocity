package info.gamefruit.velocity.elements;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public class RenderPane extends JPanel {
	private static final long serialVersionUID = -4165794799780593394L;
	public Graphics g;
	public BufferedImage buff;
	@Override
	public void paintComponent(Graphics gg) {
		super.paintComponent(gg);
		gg.drawImage(buff, 0, 0, null);
	}
	
	public void buildBuffer(int w, int h) {
		buff = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		g = buff.createGraphics();
	}
}