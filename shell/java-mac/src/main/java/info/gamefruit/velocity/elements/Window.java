package info.gamefruit.velocity.elements;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.JFrame;

public class Window implements KeyListener, MouseListener {
	public JFrame window;
	public RenderPane pane;
	public Window(int w, int h) {
		window = new JFrame("Velocity");
		window.addKeyListener(this);
		window.addMouseListener(this);
		window.setSize(w, h);
		window.setLocationRelativeTo(null);
		pane = new RenderPane();
		pane.buildBuffer(w, h);
		window.getContentPane().add(pane);
		window.setVisible(true);
	}
	
	public Window(String name, int w, int h) {
		window = new JFrame(name);
		window.addKeyListener(this);
		window.addMouseListener(this);
		window.setSize(w, h);
		window.setLocationRelativeTo(null);
		pane = new RenderPane();
		pane.buildBuffer(w, h);
		window.getContentPane().add(pane);
		window.setVisible(true);
	}
	
	public void loadScript(File script) {
		
	}

	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
